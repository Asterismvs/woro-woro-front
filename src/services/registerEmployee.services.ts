export interface RegisterData {
  name: string;
  surname: string;
  password: string;
  branch_ID: number;
}
const setEmployeeCode = (name: string, surname: string, branch_ID: number) => {
  const first = name.slice(0, 2);
  const second = surname.slice(0, 2);
  const employeeCode = first + second + branch_ID;

  return employeeCode;
};
export const registerEmployee = async (dataEmployee: RegisterData) => {
  const { name, surname, branch_ID } = dataEmployee;
  console.log(dataEmployee);
  const dataEmployeeNew = {
    rol: "employee",
    employee_code: setEmployeeCode(name, surname, branch_ID),
    ...dataEmployee,
  };
  console.log(dataEmployeeNew);
  alert(`Your employeeCode is ${dataEmployeeNew.employee_code}`);
  const path = "https://woro-woro-back.onrender.com/employees/register";
  const response = await fetch(
    // "https://woro-woro-back.onrender.com/employees/register",
    path,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(dataEmployeeNew),
    }
  );
  alert(`EmployeeCode => ${dataEmployeeNew.employee_code} `);
  const json = await response.json();

  if (!response.ok) {
    throw new Error(json.message || "Error registering employee");
  }

  return json;
};
