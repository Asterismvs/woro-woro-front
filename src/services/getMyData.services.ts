export const getMyData = async (token) => {
  // const response = await fetch(
  //   "https://woro-woro-back.onrender.com/api/v0/employees/"
  // );
  const response = await fetch("http://localhost:4000/api/v0/employees", {
    headers: {
      Authorization: token,
    },
  });

  console.log(response);

  const json = await response.json().then((data) => data);
  console.log(json);

  if (!response.ok) {
    throw new Error(json.message);
  }
  console.log(json.data);

  return json.data;
};
