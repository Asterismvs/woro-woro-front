export const getAllEmployees = async () => {
  // const response = await fetch(
  //   "https://woro-woro-back.onrender.com/api/v0/employees/"
  // );
  const response = await fetch("http://localhost:4000/api/v0/employees");

  // console.log(response);

  const json = await response.json().then((data) => data);
  // console.log(json);

  if (!response.ok) {
    throw new Error(json.message);
  }
  console.log(json.data);

  return json.data;
};
