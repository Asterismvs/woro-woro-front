export const getEmployeeId = async (id: number | string) => {
  // const response = await fetch(
  //   "https://woro-woro-back.onrender.com/api/v0/employees/"
  // );
  const response = await fetch(`http://localhost:4000/api/v0/employees/${id}`);

  const json = await response.json().then((data) => data);
  console.log(json);

  if (!response.ok) {
    throw new Error(json.message);
  }
  console.log(json.data);

  return json.data;
};
