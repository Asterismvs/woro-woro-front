export const login = async (data: { employeeCode; password }) => {
  // const response = await fetch(
  //   "https://woro-woro-back.onrender.com/api/v0/employees/"
  // );

  const path =    "https://woro-woro-back.onrender.com/api/v0/employees/";
  const response = await fetch(path, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  });

  console.log(response);

  const json = await response.json();
  console.log(json);

  if (!response.ok) {
    throw new Error(json.message);
  }
  console.log(json.data);

  return json.data;
};
