export interface Employee {
  employee_code: string;
  password: string;
  name: string;
  surname: string;
  branch_ID: number;
  created_at: string;
}

export interface EmployeesProps {
  isLoading: boolean;
}
