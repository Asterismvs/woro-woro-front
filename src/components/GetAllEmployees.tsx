import { Employee } from "../interfaces/employees.interfaces";
import { getAllEmployees } from "../services/getAllEmployees.services";
import "./EmployeeList.css";

export const GetAllEmployees = () => {
  // if () {
  //   return <div>Loading...</div>;
  // }

  const employees = getAllEmployees();
  return (
    <>
      {employees.then((data) =>
        data.map((employee: Employee) => (
          <ul key={employee.employee_code}>
            <li>{employee.name}</li>
            <li>{employee.surname}</li>
            <li>{employee.employee_code}</li>
            <li>{employee.branch_ID}</li>
            <li>X</li>
          </ul>
        ))
      )}
    </>
  );
};
