import { createSlice } from "@reduxjs/toolkit";

export const employeeSlice = createSlice({
  name: "employeeActive",
  initialState: {
    value: {
      ID: 0,
      employee_code: "",
      name: "",
      surname: "",
      branch_ID: 0,
    },
  },

  reducers: {
    activedEmployee: (state, action) => {
      //  state.push(action.payload);
      console.log(action.payload);
      state.value = action.payload;
    },
    showActivedEmployee: (state) => {
      return state;
    },
  },
});

export const { activedEmployee, showActivedEmployee } = employeeSlice.actions;

export default employeeSlice.reducer;
