import { createContext, useEffect, useState } from "react";
import { getMyData } from "../services/getMyData.services";

export const AuthContext = createContext(null);

export const AuthContextProviderComponent = ({ children }) => {
  const [token, setToken] = useState<string>(localStorage.getItem("token"));
  const [employee, setEmployee] = useState<object>(null);

  useEffect(() => {
    localStorage.setItem("token", token);
  }, [token]);

  useEffect(() => {
    const getEmployeeData = async () => {
      try {
        const data = await getMyData(token);
        setEmployee(data);
      } catch (error) {
        setToken("");
        setEmployee(null);
      }
    };
    if (token) getEmployeeData();
  }, [token, setToken]);

  const logOut = () => {
    setToken("");
    setEmployee(null);
  };

  const logIn = () => {
    setToken(token);
  };
  return (
    <AuthContext.Provider value={{ token, logIn, logOut, employee }}>
      {children}
    </AuthContext.Provider>
  );
};
