import { Suspense } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import "./App.css";
import AppAcces from "./pages/AppAcces.page";
import { HomePage } from "./pages/Home.page";
import { LogIn } from "./pages/LogIn.page";
// import { Employees } from "./pages/Employees.page";
import { Branches } from "./pages/Branches.page";
import { Schedules } from "./pages/Schedule.page";
import { CommingSuun } from "./pages/CommingSuun.page";

import { Register } from "./pages/Register.page";

function App() {
  const dateNow = new Date().toLocaleString();
  const navigate = useNavigate();
  return (
    <Suspense fallback={<h1>LOADING::::</h1>}>
      <nav>
        <ul>
          <li>Branch 99</li>
          <li>Jorgo Jorgidrez</li>
        </ul>
        <ul
          style={{
            border: "1px solid green",
            display: "flex",
            listStyle: " none ",
            placeContent: "space-between",
          }}
        >
          <li
            onClick={() => {
              navigate("/");
            }}
            style={{ cursor: "pointer" }}
          >
            HOME
          </li>
          <li>{dateNow}</li>
          <li>messages</li>
          <li>logout</li>
        </ul>
      </nav>
      <Routes>
        <Route path="/" element={<AppAcces />} />
        <Route path="/login" element={<LogIn />} />
        <Route path="/register" element={<Register />} />

        <Route path="/home" element={<HomePage />} />

        {/* <Route path="/employees" element={<Employees />} /> */}
        <Route path="/branches" element={<Branches />} />
        <Route path="/schedules" element={<Schedules />} />
        <Route path="/commingsuun" element={<CommingSuun />} />

        {/* <Route path="/" element={<Home />} />   */}
        {/* <Route path="/login" element={<LogIn />} />  */}
        {/* <Route path="/register" element={<Register />} /> */}
        {/* <Route path="/employees" element={<Employees />} /> */}
      </Routes>
    </Suspense>
  );
}

export default App;
