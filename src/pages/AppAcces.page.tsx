import { useNavigate } from "react-router-dom";

const AppAcces = () => {
  const navigate = useNavigate();

  const handleFormAccess = (event: React.FormEvent) => {
    event.preventDefault();

    navigate("/login");
  };

  return (
    <>
      <h1>SOY LA PAGINA DE ACCESO</h1>
      <form onSubmit={handleFormAccess}>
        <fieldset>
          <label htmlFor="password">AccessCode</label>
          <input
            type="password"
            name="password"
            id="password"
            placeholder="Acces Code"
          />
        </fieldset>
        <input type="submit" value="Access" />
      </form>
    </>
  );
};

export default AppAcces;
