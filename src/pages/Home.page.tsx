import { useNavigate } from "react-router-dom";

export const HomePage = () => {
  const navigate = useNavigate();
  return (
    <>
      <h1>SOY LA HOME PAGE</h1>
      <ul
        style={{
          width: "75dvw",
          height: "40dvh",
          border: "1px solid green",
          display: "flex",
          flexWrap: "wrap",
          listStyle: " none ",
        }}
      >
        <li
          style={{
            width: "50%",
            height: "100%",
            border: "1px dashed yellow",
            cursor: "pointer",
          }}
          onClick={() => {
            navigate("/employees");
          }}
        >
          employees
        </li>
        <li
          style={{
            width: "50%",
            height: "100%",
            border: "1px dashed yellow",
            cursor: "pointer",
          }}
          onClick={() => {
            navigate("/branches");
          }}
        >
          branches
        </li>
        <li
          style={{
            width: "50%",
            height: "100%",
            border: "1px dashed yellow",
            cursor: "pointer",
          }}
          onClick={() => {
            navigate("/schedules");
          }}
        >
          schedule
        </li>
        <li
          style={{
            width: "50%",
            height: "100%",
            border: "1px dashed yellow",
            cursor: "pointer",
          }}
          onClick={() => {
            navigate("/commingsuun");
          }}
        >
          coming soon
        </li>
      </ul>
    </>
  );
};
