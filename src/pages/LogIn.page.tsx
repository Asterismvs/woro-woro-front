import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { login } from "../services/login.services";
import { AuthContext } from "../context/AuthContext";

export const LogIn: React.FC = () => {
  const [employeeCode, setEmployeeCode] = useState<string>("test");
  const [password, setPassword] = useState<string>("test");
  const { logIn } = useContext(AuthContext);
  const navigate = useNavigate();

  const handleFormLogin = async (event: React.FormEvent) => {
    event.preventDefault();
    console.log({ employeeCode, password });

    try {
      const token = await login({ employeeCode, password });
      logIn(token);
      navigate("/home");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <h1>SOY LA PAGINA DE LOGIN</h1>

      <form onSubmit={handleFormLogin}>
        <fieldset>
          <label htmlFor="employeeCode">Employee Code</label>
          <input
            type="text"
            name="employeeCode"
            id="employeeCode"
            placeholder="Employee Code"
            value={employeeCode}
            onChange={(event) => {
              setEmployeeCode(event.target.value);
            }}
          />
          <label htmlFor="password">AccessCode</label>
          <input
            type="password"
            name="password"
            id="password"
            placeholder="Password"
            value={password}
            onChange={(event) => {
              setPassword(event.target.value);
            }}
          />
        </fieldset>
        <input type="submit" value="Access" />
      </form>
    </>
  );
};
