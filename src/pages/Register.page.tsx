import { useState } from "react";
import {
  RegisterData,
  registerEmployee,
} from "../services/registerEmployee.services";
import { useNavigate } from "react-router-dom";
export const Register: React.FC = () => {
  const [name, setName] = useState<string>("");
  const [surname, setSurname] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [branch_ID, setBranch_ID] = useState<number>(0);

  const navigate = useNavigate();
  const handleFormRegister = (event: React.FormEvent) => {
    event.preventDefault();
    const newEmployee: RegisterData = { name, surname, password, branch_ID };

    registerEmployee(newEmployee);
    navigate("/login");
  };

  return (
    <>
      <form onSubmit={handleFormRegister}>
        <fieldset>
          <label htmlFor="name">Name</label>
          <input
            type="text"
            name="name"
            id="name"
            onChange={(event) => {
              setName(event.target.value);
            }}
          />
          <label htmlFor="surname">Surname</label>
          <input
            type="text"
            name="surname"
            id="surname"
            onChange={(event) => {
              setSurname(event.target.value);
            }}
          />
          <label htmlFor="password">Password</label>
          <input
            type="password"
            name="password"
            id="password"
            onChange={(event) => {
              setPassword(event.target.value);
            }}
          />
          <label htmlFor="branch_ID">Branch Number</label>
          <input
            type="number"
            name="branch_ID"
            id="branch_ID"
            onChange={(event) => {
              setBranch_ID(Number(event.target.value));
            }}
          />
        </fieldset>

        <input type="submit" value="Register" />
      </form>
    </>
  );
};
