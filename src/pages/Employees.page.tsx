import { useEffect, useState } from "react";
import { getEmployeeId } from "../services/getEmployeeId.services";
import { GetAllEmployees } from "../components/GetAllEmployees";

interface EmployeeProps {}
export const Employees: React.FC<EmployeeProps> = () => {
  const [isEdit, setIsEdit] = useState<boolean>(true);
  const [id, setId] = useState<number | string>(0);
  const [employeeId, setEmployeeId] = useState<string[]>([]);
  const employeeCode = "COeDIGO";

  const coso = (value: string) => {
    if (isEdit) {
      return <p>{value}</p>;
    } else {
      return <input defaultValue={value}></input>;
    }
  };
  const editClick = (event: React.MouseEvent<HTMLElement>) => {
    event.preventDefault();
    setIsEdit(false);
    setTimeout(() => {
      setIsEdit(true);
    }, 5000);
  };

  useEffect(() => {
    const fetcHData = async () => {
      const { employee_code, password, name, surname, branch_ID } =
        await getEmployeeId(id);

      setEmployeeId([employee_code, password, name, surname, branch_ID]);
    };
    fetcHData();
  }, [id]);

  return (
    <>
      <h1>EMPLOYEEEEES PAGE</h1>

      <section>
        <h2>ESTA ES LA LISTA DE LA SUCURSAL {"NOMBRE SUCURSAL ACTUAL"}</h2>
        <nav>
          <ul
            style={{
              border: "1px solid yellow",
              display: "flex",
              placeContent: "space-evenly",
              listStyle: " none ",
            }}
          >
            <li>
              <label htmlFor="search">Search</label>
              <input
                type="search"
                name="search"
                id="search"
                placeholder="Search"
              />
              <input type="button" value="search" />
            </li>
            <li>NEW</li>
            <li>EDIT</li>
            <li>DELETE</li>
          </ul>
        </nav>
        <ul
          style={{
            border: "1px solid green",
            display: "flex",
            placeContent: "space-around",
            listStyle: " none ",
          }}
        >
          <li>
            <strong>Nombre</strong>
          </li>
          <li>
            <strong>Apellido</strong>
          </li>
          <li>
            <strong>Sucursal</strong>
          </li>
        </ul>
        <ul
          style={{
            border: "1px solid green",
            display: "flex",
            placeContent: "space-around",
            listStyle: " none ",
          }}
        >
          <li onClick={editClick} className="pepe">
            {coso("Unnombre")}
          </li>

          <li>Unapellido</li>
          <li>Unsucursal</li>
        </ul>
        <ul
          style={{
            border: "1px solid green",
            display: "flex",
            placeContent: "space-around",
            listStyle: " none ",
          }}
        >
          <li>Otronombre</li>
          <li>Otroapellido</li>
          <li>Otrosucursal</li>
        </ul>
      </section>
      <p style={{ color: "lightcoral" }}>
        Si buscamos, el componente rensderiza solo la busqueda
      </p>
      <section>
        <nav>
          <ul
            style={{
              border: "1px solid yellow",
              display: "flex",
              placeContent: "space-evenly",
              listStyle: " none ",
            }}
          >
            <li>
              <label htmlFor="search">Search</label>
              <input
                type="search"
                name="search"
                id="search"
                placeholder="Search"
                value={"unnombre"}
              />
              <input type="button" value="search" />
            </li>
            <li>NEW</li>
            <li>EDIT</li>
            <li>DELETE</li>
          </ul>
        </nav>
        <ul
          style={{
            border: "1px solid green",
            display: "flex",
            placeContent: "space-around",
            listStyle: " none ",
          }}
        >
          <li>
            <strong>Nombre</strong>
          </li>
          <li>
            <strong>Apellido</strong>
          </li>
          <li>
            <strong>Sucursal</strong>
          </li>
        </ul>
        <ul
          style={{
            border: "1px solid green",
            display: "flex",
            placeContent: "space-around",
            listStyle: " none ",
          }}
        >
          <li>Unnombre</li>
          <li>Unapellido</li>
          <li>Unsucursal</li>
        </ul>
      </section>
      <p style={{ color: "lightcoral" }}>
        Al apretar el boton de NEW sne rensderizva este componente
      </p>
      <section>
        <form onSubmit={() => {}}>
          <fieldset>
            <label htmlFor="employeeCode">Employee Code</label>
            <input
              type="text"
              name="employeeCode"
              id="employeeCode"
              value={employeeCode}
              disabled
            />
            <label htmlFor="name">Name</label>
            <input type="text" name="name" id="name" />
            <label htmlFor="surname">Surname</label>
            <input type="text" name="surname" id="surname" />

            <label htmlFor="branch">Branch</label>
            <select name="branch" id="branch">
              <option value="1">Ciudad Uno</option>
              <option value="2">Ciudad Dos</option>
            </select>
          </fieldset>
          <input type="submit" value="add employfee" />
        </form>
      </section>
      <p style={{ color: "lightcoral" }}>Por Id EMployee</p>

      <section>
        <input
          type="number"
          name="id"
          id="id"
          onChange={(event) => {
            setId(event.target.value);
          }}
        />
        <input type="submit" value="ID" />
        <h1>{id}</h1>
        <ul>
          {employeeId.map((data, index) => {
            return <li key={index}>{data}</li>;
          })}
        </ul>
      </section>
      <p>Otra cosa mariposa</p>
      <section>
        <ul>
          <li>
            <p style={{ color: "yellow" }}>Esto es un campo</p>
            <p>⁂</p>
          </li>
        </ul>
      </section>
      <GetAllEmployees />
    </>
  );
};
